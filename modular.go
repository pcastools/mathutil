// Modular implements safe modular arithmetic for int64

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mathutil

import (
	"fmt"
	"math"
	"math/big"
	"math/bits"
)

// The maximum number of elements in a slice before we compute the sum or product concurrently.
const (
	maxModularSumLength        = 1000
	maxModularProductLength    = 1000
	maxModularDotProductLength = 500
)

//////////////////////////////////////////////////////////////////////
// multiplication and exponentiation functions
//////////////////////////////////////////////////////////////////////

/*
Schrage's method, explained on page 278 of

   "Numerical Recipes in C: The Art of Scientific Computing", Second Edition, by Press, Teukolsky, Vetterling, and Flannery,

allows us to multiply two int64s modulo an int64 in such a way that all quantities required by the calculation will fit in a uint64. This works as follows. Our goal is to compute

a*z mod m

where we assume without loss of generality that a and z lie in [1,m-1]. Schrage's method requires also that a^2 <= m. After setting
q = [m/a] -- the floor of m/a -- and r = m mod a, this implies that r<q. (Proof: a^2 <= m, so a <= m/a, so a <= [m/a]. But r<a.)  Thus

a*(z mod q)     and     r*[z/q]

are both in [0,m]. And:

a*z mod m = a*(z mod q) - r*[z/q]        if this is non-negative

or        = a*(z mod q) - r*[z/q] + m    otherwise.

So we can compute this inside a uint64.

If a^2 > m then m is reasonably small. So if, for instance, a<=2^21 then m <= 2^42

(a mod m) * (z mod m)  is at most  2*21 * 2^42  which fits in an int64

If a^2>m and a > 2^21 then we fall back to the standard math/big Go big integer library.
*/

// MulModInt64 returns a*z mod m. m is required to be positive; this will panic otherwise. The return value is always in the range [0,m-1].
func MulModInt64(a int64, z int64, m int64) int64 {
	// Sanity check
	if m <= 0 {
		panic("Argument 3 must be a positive integer.")
	}
	// Do the trivial case
	if m == 1 {
		return 0
	}
	// Reduce a and z modulo m, and get the signs right (note that Go uses
	// computer science conventions for %, not mathematical conventions, so we
	// need to correct for signs).
	if a = a % m; a < 0 {
		a += m
	}
	if z = z % m; z < 0 {
		z += m
	}
	// Make sure that a is the smaller of a, z
	if a > z {
		a, z = z, a
	}
	// Do the easy cases
	if z <= math.MaxUint32 {
		return int64((uint64(a) * uint64(z)) % uint64(m))
	} else if a == 0 {
		return 0
	}
	// Can we use Schrage's method?
	if a <= math.MaxInt32 && a*a <= m {
		// Calculate q and r  (r is automatically of the correct sign)
		q := m / a
		r := m - a*q
		// We need [z/q] and z mod m, too. (Again, the sign of z%q is
		// automatically correct.)
		zDivq := z / q
		zModq := z - q*zDivq
		// Perform the multiplication
		firstTerm, secondTerm := a*zModq, r*zDivq
		if firstTerm >= secondTerm {
			return firstTerm - secondTerm
		}
		secondTerm = m - secondTerm
		return firstTerm + secondTerm
	}
	// Gall back to Go big integers
	aa := pool.Get().(*big.Int).SetInt64(a)
	t := pool.Get().(*big.Int).SetInt64(z)
	aa.Mul(aa, t)
	n := aa.Mod(aa, t.SetInt64(m)).Int64()
	pool.Put(t)
	pool.Put(aa)
	return n
}

// MulModInt64Trial returns a*b mod m. m is required to be positive; this will panic otherwise. The return value is always in the range [0,m-1].
func MulModInt64Trial(a int64, b int64, m int64) int64 {
	if m <= 0 {
		panic("Argument 3 must be a positive integer.")
	}
	// do the trivial case
	if m == 1 {
		return 0
	}
	// reduce a and b modulo m, and get the signs right
	// Go uses computer science conventions for %, not mathematical conventions
	if a = a % m; a < 0 {
		a += m
	}
	if b = b % m; b < 0 {
		b += m
	}
	// do the easy cases
	if a == 0 {
		return 0
	} else if a == 1 {
		return b
	} else if b == m-1 {
		return m - a
	}
	// use the second algorithm from
	// https://en.wikipedia.org/wiki/Modular_arithmetic#Example_implementations
	fmt.Printf("MulModInt64: a=%v, b=%v, m=%v\n", a, b, m)
	x := float64(a)
	c := uint64(x * float64(b) / float64(m))
	r := int64(uint64(a)*uint64(b)-c*uint64(m)) % m
	if r < 0 {
		r += m
	}
	return r
}

// ExpModInt64 returns a^k mod m. m is required to be positive; this will panic otherwise. k is required to be non-negative; this will panic otherwise. The return value is always in the range [0,m-1].
func ExpModInt64(a int64, k int64, m int64) int64 {
	// sanity checks
	if k < 0 {
		panic("Argument 2 must be a non-negative integer.")
	}
	if m <= 0 {
		panic("Argument 3 must be a positive integer.")
	}
	// do the trivial case
	if m == 1 {
		return 0
	}
	// reduce a mod m, getting the sign right
	if a = a % m; a < 0 {
		a += m
	}
	// do the easy cases
	if a == 1 || k == 0 {
		return 1
	}
	if a == 0 {
		return 0
	}
	if k == 1 {
		return a
	}
	if a == m-1 {
		if k%2 == 0 {
			return 1
		}
		return m - 1
	}
	// Do the exponentiation
	res := int64(1)
	pow := a
	for {
		if k&1 == 1 {
			res = MulModInt64(res, pow, m)
		}
		// Move on
		k = k >> 1
		if k == 0 {
			return res
		}
		pow = MulModInt64(pow, pow, m)
	}
}

//////////////////////////////////////////////////////////////////////
// addition and remainder functions
//////////////////////////////////////////////////////////////////////

// AddModInt64 returns a+b modulo m. The modulus m is required to be positive; this will panic otherwise.
func AddModInt64(a int64, b int64, m int64) int64 {
	if m <= 0 {
		panic("Argument 3 must be a positive integer.")
	}
	// reduce a and b modulo m, getting the signs right
	// Go uses computer science conventions for %, not mathematical conventions
	if a = a % m; a < 0 {
		a += m
	}
	if b = b % m; b < 0 {
		b += m
	}
	return int64((uint64(a) + uint64(b)) % uint64(m))
}

// DivModInt64 returns q, r where a = q b+r and 0 <= r < b.
// We require that b is positive, and panic if this is not the case
func DivModInt64(a int64, b int64) (int64, int64) {
	if b <= 0 {
		panic("Argument 2 must be a positive integer.")
	}
	// Recall that Go's % operator is not the Euclidean modulus:
	// see [https://golang.org/ref/spec#Arithmetic_operators]
	r := a % b
	if r < 0 {
		r += b
	}
	q := (a - r) / b
	return q, r
}

// XGCDOfPairInt64 returns the greatest common divisor of a and b, along with integers u and v such that u a + v b = gcd. The gcd is always non-negative. The gcd of 0 and n is defined to be |n|, so in particular, the gcd of 0 and 0 is 0. We require that a and b are both greater than math.MinInt64 (=-2^63) as otherwise the calculation could overflow an int64; it panics in this case.
func XGCDOfPairInt64(a int64, b int64) (int64, int64, int64) {
	if a == math.MinInt64 || b == math.MinInt64 {
		panic("Potential overflow in XGCDOfPairInt64. Both inputs were -2^63, which is not allowed.")
	}
	// at this point we can safely change the sign of a and b if they are negative
	// (it can't overflow because we know that a and b are both > -2^63, so
	// |a| and |b| are both at most 2^63-1 = math.MaxInt64)
	aNeg := false
	if a < 0 {
		a = -a
		aNeg = true
	}
	bNeg := false
	if b < 0 {
		b = -b
		bNeg = true
	}
	// get the zero cases right
	if a == 0 {
		if bNeg {
			return b, 0, -1
		}
		return b, 0, 1
	} else if b == 0 {
		if aNeg {
			return a, -1, 0
		}
		return a, 1, 0
	}
	// this is https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
	var r0, r1, s0, s1, t0, t1 int64
	r0 = a
	r1 = b
	s0 = 1
	s1 = 0
	t0 = 0
	t1 = 1
	q, rnext := DivModInt64(r0, r1)
	snext := s0 - q*s1
	tnext := t0 - q*t1
	for rnext != 0 {
		r0, s0, t0 = r1, s1, t1
		r1, s1, t1 = rnext, snext, tnext
		q, rnext = DivModInt64(r0, r1)
		snext = s0 - q*s1
		tnext = t0 - q*t1
	}
	if aNeg {
		s1 *= -1
	}
	if bNeg {
		t1 *= -1
	}
	return r1, s1, t1
}

//////////////////////////////////////////////////////////////////////
// Multiply then add/subtract functions
//////////////////////////////////////////////////////////////////////

// MulThenAddModInt64 returns a*b+c modulo m. The modulus m is required to be positive; this will panic otherwise.
func MulThenAddModInt64(a int64, b int64, c int64, m int64) int64 {
	if m <= 0 {
		panic("Argument 4 must be a positive integer.")
	}
	return AddModInt64(MulModInt64(a, b, m), c, m)
}

//////////////////////////////////////////////////////////////////////
// Sum functions
//////////////////////////////////////////////////////////////////////

// sumInt64ModInt64Internal passes the sum of S, computed modulo m, to the channel c
func sumInt64ModInt64Internal(c chan<- int64, S []int64, m int64) {
	c <- SumInt64ModInt64(S, m)
}

// SumInt64ModInt64 returns the sum of S computed modulo m. The sum of the empty slice is zero.
func SumInt64ModInt64(S []int64, m int64) int64 {
	var result int64
	if len(S) <= maxModularSumLength {
		// just add everything up
		result = 0
		for _, x := range S {
			result = AddModInt64(result, x, m)
		}
		return result
	}
	// otherwise do a recursive split
	c := make(chan int64)
	div := len(S) / 2
	go sumInt64ModInt64Internal(c, S[:div], m)
	go sumInt64ModInt64Internal(c, S[div:], m)
	r := <-c
	l := <-c
	return AddModInt64(r, l, m)
}

//////////////////////////////////////////////////////////////////////
// Product functions
//////////////////////////////////////////////////////////////////////

// productInt64ModInt64Internal passes the product of S, computed modulo m, to the channel c
func productInt64ModInt64Internal(c chan<- int64, S []int64, m int64) {
	c <- ProductInt64ModInt64(S, m)
}

// ProductInt64ModInt64 returns the product of S computed modulo m. The product of the empty slice is 1.
func ProductInt64ModInt64(S []int64, m int64) int64 {
	if len(S) <= maxModularProductLength {
		// just multiply everything
		result := int64(1)
		for _, x := range S {
			result = MulModInt64(result, x, m)
		}
		return result
	}
	// otherwise do a recursive split
	c := make(chan int64)
	div := len(S) / 2
	go productInt64ModInt64Internal(c, S[:div], m)
	go productInt64ModInt64Internal(c, S[div:], m)
	return MulModInt64(<-c, <-c, m)
}

/////////////////////////////////////////////////////////////////////////
// DotProduct functions
/////////////////////////////////////////////////////////////////////////

// dotProductInt64ModInt64Internal passes the dot-product of the slices S1 and S2 as an int64, computed modulo m, to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductInt64ModInt64Internal(c chan<- int64, S1 []int64, S2 []int64, m int64) {
	c <- dotProductInt64ModInt64(S1, S2, m)
}

// dotProductInt64ModInt64 returns the dot-product of the slices S1 and S2 computed modulo m. The slices are assumed to be non-empty and of the same length.  m is assumed to be positive.
func dotProductInt64ModInt64(S1 []int64, S2 []int64, m int64) int64 {
	if len(S1) <= maxModularDotProductLength {
		// Calculate the dot-product
		var total int64
		if m < 1<<31 {
			// Since m is at most 2^31 - 1, we know that we can safely do x*y
			// rather than x*y mod m without overflowing an int64. We bound the
			// sizes of intermediate quantities in the computation, only
			// reducing mod m when necessary find the least integer mBits such
			// that m < 2^mBits.  We use the fact that m is non-zero here.
			mBits := uint(bits.Len64(uint64(m)))
			if m == 1<<mBits {
				mBits += 1
			}
			// Below we see that total < k*2^(2*mBits) where there are k
			// additions
			numberOfSafeAdditions := 1 << (63 - 2*mBits)
			count := 0
			// now calculate the dot product
			for i, x := range S1 {
				// check that x is in the range [0..m-1]
				if x < 0 {
					x = x%m + m
				} else if x >= m {
					x = x % m
				}
				// grab y, and check that it is in the range [0..m-1]
				y := S2[i]
				if y < 0 {
					y = y%m + m
				} else if y >= m {
					y = y % m
				}
				// add to the dot product
				if x != 0 && y != 0 {
					if count == numberOfSafeAdditions {
						// addition might overflow, so we reduce mod m
						if x == 1 {
							total = AddModInt64(total, y, m)
						} else if y == 1 {
							total = AddModInt64(total, x, m)
						} else {
							total = AddModInt64(total, x*y, m)
						}
						count = 0
					} else {
						// addition is safe, so we don't reduce mod m
						if x == 1 {
							total += y
						} else if y == 1 {
							total += x
						} else {
							total += x * y
						}
					}
				}
			}
			return total % m
		}
		// m is at least 2^32, so we need to reduce mod m at every step
		for i, x := range S1 {
			y := S2[i]
			if x != 0 && y != 0 {
				if x == 1 {
					total = AddModInt64(total, y, m)
				} else if y == 1 {
					total = AddModInt64(total, x, m)
				} else {
					total = AddModInt64(total, MulModInt64(x, y, m), m)
				}
			}
		}
		return total
	}
	// Perform a recursive split
	c := make(chan int64)
	div := len(S1) / 2
	go dotProductInt64ModInt64Internal(c, S1[:div], S2[:div], m)
	go dotProductInt64ModInt64Internal(c, S1[div:], S2[div:], m)
	return AddModInt64(<-c, <-c, m)
}

// DotProductInt64ModInt64 returns the dot-product of the slices S1 and S2, computed modulo m. That is, it returns S1[0] * S2[0] + ... + S1[n] * S2[n] mod m, where n+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.  DotProductInt64ModInt64 will panic if m is not positive.
func DotProductInt64ModInt64(S1 []int64, S2 []int64, m int64) int64 {
	// Sanity check
	if m <= 0 {
		panic("The third argument to DotProductInt64ModInt64 must be positive.")
	}
	// Resize the slices so that they both have the same length
	n := len(S1)
	if n2 := len(S2); n != n2 {
		if n < n2 {
			S2 = S2[:n]
		} else {
			n = n2
			S1 = S1[:n]
		}
	}
	// Fast-track the easy cases
	if n <= 1 {
		if n == 0 {
			return 0
		} else if n == 1 {
			return MulModInt64(S1[0], S2[0], m)
		}
		return AddModInt64(MulModInt64(S1[0], S2[0], m), MulModInt64(S1[1], S2[1], m), m)
	}
	// Calculate the dot-product
	return dotProductInt64ModInt64(S1, S2, m)
}
