// Dotproduct implements an efficient dot-product.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mathutil

import (
	"math"
	"math/big"
	"sync"
)

// pool is a pool of *big.Ints.
var pool = &sync.Pool{
	New: func() interface{} {
		return &big.Int{}
	},
}

// maxInt64AsBigInt is the maximum value of an int64, represented as a *big.Int. You should treat this value as a constant.
var maxInt64AsBigInt = big.NewInt(math.MaxInt64)

// maxInt64DotProd is the maximum number of elements in the slice before we compute the dot-product of two slices of int64s concurrently.
const maxInt64DotProd = 8000

// int64OrBigInt contains either an int64 or a *big.Int value.
type int64OrBigInt struct {
	n int64
	k *big.Int
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// castToInt64Pair attempts to cast the given *big.Int to an int64. If this is possible, the *big.Int will be returned to the pool before returning. If this is not possible then the min/max value in the range will be returned, along with the *big.Int.
func castToInt64Pair(a *big.Int) (int64, *big.Int) {
	if a.IsInt64() {
		n := a.Int64()
		pool.Put(a)
		return n, nil
	} else if a.Cmp(maxInt64AsBigInt) > 0 {
		return math.MaxInt64, a
	}
	return math.MinInt64, a
}

// castInt64PairToInt32Pair attempts to cast the given pair of int64 and *big.Int to an int32. If this is possible, the *big.Int will be returned to the pool before returning. If this is not possible then the min/max value in the range will be returned, along with the *big.Int.
func castInt64PairToInt32Pair(n int64, k *big.Int) (int32, *big.Int) {
	if k == nil {
		if n > math.MaxInt32 {
			k = pool.Get().(*big.Int).SetInt64(n)
			n = math.MaxInt32
		} else if n < math.MinInt32 {
			k = pool.Get().(*big.Int).SetInt64(n)
			n = math.MinInt32
		}
	} else if n == math.MaxInt64 {
		n = math.MaxInt32
	} else {
		n = math.MinInt32
	}
	return int32(n), k
}

// castInt64PairToInt16Pair attempts to cast the given pair of int64 and *big.Int to an int16. If this is possible, the *big.Int will be returned to the pool before returning. If this is not possible then the min/max value in the range will be returned, along with the *big.Int.
func castInt64PairToInt16Pair(n int64, k *big.Int) (int16, *big.Int) {
	if k == nil {
		if n > math.MaxInt16 {
			k = pool.Get().(*big.Int).SetInt64(n)
			n = math.MaxInt16
		} else if n < math.MinInt16 {
			k = pool.Get().(*big.Int).SetInt64(n)
			n = math.MinInt16
		}
	} else if n == math.MaxInt64 {
		n = math.MaxInt16
	} else {
		n = math.MinInt16
	}
	return int16(n), k
}

// multiplyInt64ThenAddInt64 computes a*b + c. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int.
func multiplyInt64ThenAddInt64(a int64, b int64, c int64) (int64, *big.Int) {
	// First compute r = a * b
	r, ok := MultiplyInt64(a, b)
	if !ok {
		// No use -- we might as well do the entire computation using *big.Ints
		aa, bb := pool.Get().(*big.Int).SetInt64(a), pool.Get().(*big.Int).SetInt64(b)
		aa.Mul(aa, bb)
		aa.Add(aa, bb.SetInt64(c))
		pool.Put(bb)
		return castToInt64Pair(aa)
	}
	// Now compute r + c
	if s, ok := AddInt64(r, c); ok {
		return s, nil
	}
	// No use -- we need to do this addition using *big.Ints
	aa, bb := pool.Get().(*big.Int).SetInt64(r), pool.Get().(*big.Int).SetInt64(c)
	aa.Add(aa, bb)
	pool.Put(bb)
	return castToInt64Pair(aa)
}

// multiplyInt64ThenAddBigInt computes a*b + c. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int. The given *big.Int c may be reused in the computation, and will be returned to to pool before returning.
func multiplyInt64ThenAddBigInt(a int64, b int64, c *big.Int) (int64, *big.Int) {
	// First compute r = a * b
	r, ok := MultiplyInt64(a, b)
	if !ok {
		// No use -- we might as well do the entire computation using *big.Ints
		aa, bb := pool.Get().(*big.Int).SetInt64(a), pool.Get().(*big.Int).SetInt64(b)
		c.Add(aa.Mul(aa, bb), c)
		pool.Put(aa)
		pool.Put(bb)
		return castToInt64Pair(c)
	}
	// Now compute r + c
	aa := pool.Get().(*big.Int).SetInt64(r)
	c.Add(c, aa)
	pool.Put(aa)
	return castToInt64Pair(c)
}

// multiplyInt32ThenAddInt64 computes a*b + c. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int.
func multiplyInt32ThenAddInt64(a int32, b int32, c int64) (int64, *big.Int) {
	// First compute r = a * b
	r := int64(a) * int64(b)
	// Now compute r + c
	if s, ok := AddInt64(r, c); ok {
		return s, nil
	}
	// No use -- we need to do this addition using *big.Ints
	aa, bb := pool.Get().(*big.Int).SetInt64(r), pool.Get().(*big.Int).SetInt64(c)
	aa.Add(aa, bb)
	pool.Put(bb)
	return castToInt64Pair(aa)
}

// multiplyInt32ThenAddBigInt computes a*b + c. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int. The given *big.Int c may be reused in the computation, and will be returned to to pool before returning.
func multiplyInt32ThenAddBigInt(a int32, b int32, c *big.Int) (int64, *big.Int) {
	ab := pool.Get().(*big.Int).SetInt64(int64(a) * int64(b))
	c.Add(c, ab)
	pool.Put(ab)
	return castToInt64Pair(c)
}

// mergeDotProductResults
func mergeDotProductResults(c <-chan *int64OrBigInt) (int64, *big.Int) {
	// Extract the results
	res1, res2 := <-c, <-c
	// Sum the results
	if res1.k == nil {
		if res2.k == nil {
			if n, ok := AddInt64(res1.n, res2.n); ok {
				return n, nil
			}
			a, b := pool.Get().(*big.Int).SetInt64(res1.n), pool.Get().(*big.Int).SetInt64(res2.n)
			a.Add(a, b)
			pool.Put(b)
			return castToInt64Pair(a)
		}
		a, b := pool.Get().(*big.Int).SetInt64(res1.n), res2.k
		b.Add(a, b)
		pool.Put(a)
		return castToInt64Pair(b)
	} else if res2.k == nil {
		a, b := res1.k, pool.Get().(*big.Int).SetInt64(res2.n)
		a.Add(a, b)
		pool.Put(b)
		return castToInt64Pair(a)
	}
	a, b := res1.k, res2.k
	a.Add(a, b)
	pool.Put(b)
	return castToInt64Pair(a)
}

// dotProductForInt64SliceInternal passes the dot-product of the slices S1 and S2 as to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductForInt64SliceInternal(c chan<- *int64OrBigInt, S1 []int64, S2 []int64) {
	n, k := dotProductForInt64Slice(S1, S2)
	c <- &int64OrBigInt{
		n: n,
		k: k,
	}
}

// dotProductForInt64Slice returns the dot-product for the given slices S1 and S2 of int64s. The slices are assumed to be non-empty and of the same length. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int.
func dotProductForInt64Slice(S1 []int64, S2 []int64) (n int64, k *big.Int) {
	// If the length is short enough, do this in-place
	if len(S1) < maxInt64DotProd {
		// Compute the dot-product
		for i, a := range S1 {
			if a != 0 {
				if b := S2[i]; b != 0 {
					if k == nil {
						n, k = multiplyInt64ThenAddInt64(a, b, n)
					} else {
						n, k = multiplyInt64ThenAddBigInt(a, b, k)
					}
				}
			}
		}
		return
	}
	// Perform a recursive split
	c := make(chan *int64OrBigInt)
	div := len(S1) / 2
	go dotProductForInt64SliceInternal(c, S1[:div], S2[:div])
	go dotProductForInt64SliceInternal(c, S1[div:], S2[div:])
	// Merge the results
	return mergeDotProductResults(c)
}

// dotProductForInt32SliceInternal passes the dot-product of the slices S1 and S2 as to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductForInt32SliceInternal(c chan<- *int64OrBigInt, S1 []int32, S2 []int32) {
	n, k := dotProductForInt32Slice(S1, S2)
	c <- &int64OrBigInt{
		n: n,
		k: k,
	}
}

// dotProductForInt32Slice returns the dot-product for the given slices S1 and S2 of int32s. The slices are assumed to be non-empty and of the same length. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int.
func dotProductForInt32Slice(S1 []int32, S2 []int32) (n int64, k *big.Int) {
	// If the length is short enough, do this in-place
	if len(S1) < maxInt64DotProd {
		// Compute the dot-product
		for i, a := range S1 {
			if a != 0 {
				if b := S2[i]; b != 0 {
					if k == nil {
						n, k = multiplyInt32ThenAddInt64(a, b, n)
					} else {
						n, k = multiplyInt32ThenAddBigInt(a, b, k)
					}
				}
			}
		}
		return
	}
	// Perform a recursive split
	c := make(chan *int64OrBigInt)
	div := len(S1) / 2
	go dotProductForInt32SliceInternal(c, S1[:div], S2[:div])
	go dotProductForInt32SliceInternal(c, S1[div:], S2[div:])
	// Merge the results
	return mergeDotProductResults(c)
}

// dotProductForInt16SliceInternal passes the dot-product of the slices S1 and S2 as to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductForInt16SliceInternal(c chan<- *int64OrBigInt, S1 []int16, S2 []int16) {
	n, k := dotProductForInt16Slice(S1, S2)
	c <- &int64OrBigInt{
		n: n,
		k: k,
	}
}

// dotProductForInt16Slice returns the dot-product for the given slices S1 and S2 of int16s. The slices are assumed to be non-empty and of the same length. The result is returned as an int64 if it fits in an int64, otherwise it is returned as a *big.Int.
func dotProductForInt16Slice(S1 []int16, S2 []int16) (n int64, k *big.Int) {
	// If the length is short enough, do this in-place
	if len(S1) < maxInt64DotProd {
		// Compute the dot-product
		for i, a := range S1 {
			if a != 0 {
				if b := S2[i]; b != 0 {
					if k == nil {
						n, k = multiplyInt32ThenAddInt64(int32(a), int32(b), n)
					} else {
						n, k = multiplyInt32ThenAddBigInt(int32(a), int32(b), k)
					}
				}
			}
		}
		return
	}
	// Perform a recursive split
	c := make(chan *int64OrBigInt)
	div := len(S1) / 2
	go dotProductForInt16SliceInternal(c, S1[:div], S2[:div])
	go dotProductForInt16SliceInternal(c, S1[div:], S2[div:])
	// Merge the results
	return mergeDotProductResults(c)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DotProductInt64 attempts to return the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is 0. If the dot-product fits in an int64, then this will be returned followed by true, nil. If the dot-product does not fit in an int64, then the min/max will be returned followed by false, x. The argument x is an optional *big.Int which will be set to the value of the dot-product and returned as the third return value only if the argument x is non-nil and the result does not fit in an int64.
func DotProductInt64(S1 []int64, S2 []int64, x *big.Int) (int64, bool, *big.Int) {
	// Ensure that the slices are of the same length
	k := len(S1)
	if k2 := len(S2); k2 < k {
		k = k2
		S1 = S1[:k]
	} else if k2 > k {
		S2 = S2[:k]
	}
	// Fast-track the easy cases
	if k == 0 {
		return 0, true, nil
	} else if k == 1 {
		r, ok := MultiplyInt64(S1[0], S2[0])
		if !ok && x != nil {
			a := pool.Get().(*big.Int).SetInt64(S1[0])
			x.Mul(a, x.SetInt64(S2[0]))
			pool.Put(a)
		}
		return r, ok, x
	}
	// Perform the dot-product
	ok := true
	n, nn := dotProductForInt64Slice(S1, S2)
	if nn != nil {
		if x != nil {
			x.Set(nn)
		}
		ok = false
		pool.Put(nn)
	}
	return n, ok, x
}

// DotProductInt32 attempts to return the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is 0. If the dot-product fits in an int32, then this will be returned followed by true, nil. If the dot-product does not fit in an int32, then the min/max will be returned followed by false, x. The argument x is an optional *big.Int which will be set to the value of the dot-product and returned as the third return value only if the argument x is non-nil and the result does not fit in an int32.
func DotProductInt32(S1 []int32, S2 []int32, x *big.Int) (int32, bool, *big.Int) {
	// Ensure that the slices are of the same length
	k := len(S1)
	if k2 := len(S2); k2 < k {
		k = k2
		S1 = S1[:k]
	} else if k2 > k {
		S2 = S2[:k]
	}
	// Fast-track the easy cases
	if k == 0 {
		return 0, true, nil
	} else if k == 1 {
		r, ok := MultiplyInt32(S1[0], S2[0])
		if !ok && x != nil {
			a := pool.Get().(*big.Int).SetInt64(int64(S1[0]))
			x.Mul(a, x.SetInt64(int64(S2[0])))
			pool.Put(a)
		}
		return r, ok, x
	}
	// Perform the dot-product
	n, nn := dotProductForInt32Slice(S1, S2)
	// Cast the result back to an int32 if possible
	ok := true
	nAs32, nn := castInt64PairToInt32Pair(n, nn)
	if nn != nil {
		if x != nil {
			x.Set(nn)
		}
		ok = false
		pool.Put(nn)
	}
	return nAs32, ok, x
}

// DotProductInt16 attempts to return the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is 0. If the dot-product fits in an int16, then this will be returned followed by true, nil. If the dot-product does not fit in an int16, then the min/max will be returned followed by false, x. The argument x is an optional *big.Int which will be set to the value of the dot-product and returned as the third return value only if the argument x is non-nil and the result does not fit in an int16.
func DotProductInt16(S1 []int16, S2 []int16, x *big.Int) (int16, bool, *big.Int) {
	// Ensure that the slices are of the same length
	k := len(S1)
	if k2 := len(S2); k2 < k {
		k = k2
		S1 = S1[:k]
	} else if k2 > k {
		S2 = S2[:k]
	}
	// Fast-track the easy cases
	if k == 0 {
		return 0, true, nil
	} else if k == 1 {
		r, ok := MultiplyInt16(S1[0], S2[0])
		if !ok && x != nil {
			a := pool.Get().(*big.Int).SetInt64(int64(S1[0]))
			x.Mul(a, x.SetInt64(int64(S2[0])))
			pool.Put(a)
		}
		return r, ok, x
	}
	// Perform the dot-product
	n, nn := dotProductForInt16Slice(S1, S2)
	// Cast the result back to an int16 if possible
	ok := true
	nAs16, nn := castInt64PairToInt16Pair(n, nn)
	if nn != nil {
		if x != nil {
			x.Set(nn)
		}
		ok = false
		pool.Put(nn)
	}
	return nAs16, ok, x
}
