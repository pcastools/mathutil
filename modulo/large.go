// Large implements arithmetic modulo n for large values of n.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package modulo

import (
	"math/big"
)

// largeN implements Calculator for large values of N.
type largeN struct {
	n    uint64     // The integer to reduce by
	pow2 [96]uint64 // The powers 2^{32+i} mod n
}

/////////////////////////////////////////////////////////////////////////
// largeN functions
/////////////////////////////////////////////////////////////////////////

// newLargeN returns a Calculator for the given large integer n. Requires that n is positive, otherwise this will panic.
func newLargeN(n int64) Calculator {
	// Sanity check
	if n <= 0 {
		panic("Argument must be a positive integer")
	}
	// Calculate the powers of 2 that we need
	c := &largeN{
		n: uint64(n),
	}
	nn, two, k := big.NewInt(n), big.NewInt(1<<31), big.NewInt(0)
	for i := 0; i < len(c.pow2); i++ {
		c.pow2[i] = k.Mod(two.Lsh(two, 1), nn).Uint64()
	}
	return c
}

// multiplyReducedPow32 returns (2^32 * a) mod n. Assumes a is reduced mod n.
func (c *largeN) multiplyReducedPow32(a uint64) uint64 {
	// Make a note of n and the array of values
	n, pow2 := c.n, c.pow2
	// Calculate the product
	sum := uint64(0)
	if sum = sum + (a&0x1)*pow2[0]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[1]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[2]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[3]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[4]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[5]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[6]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[7]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[8]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[9]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[10]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[11]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[12]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[13]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[14]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[15]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[16]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[17]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[18]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[19]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[20]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[21]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[22]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[23]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[24]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[25]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[26]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[27]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[28]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[29]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[30]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[31]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[32]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[33]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[34]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[35]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[36]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[37]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[38]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[39]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[40]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[41]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[42]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[43]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[44]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[45]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[46]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[47]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[48]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[49]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[50]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[51]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[52]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[53]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[54]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[55]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[56]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[57]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[58]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[59]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[60]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[61]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[62]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[63]; sum >= n {
		sum -= n
	}
	return sum
}

// multiplyReducedPow64 returns (2^64 * a) mod n. Assumes a is reduced mod n.
func (c *largeN) multiplyReducedPow64(a uint64) uint64 {
	// Make a note of n and the array of values
	n, pow2 := c.n, c.pow2
	// Calculate the product
	sum := uint64(0)
	if sum = sum + (a&0x1)*pow2[32]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[33]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[34]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[35]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[36]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[37]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[38]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[39]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[40]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[41]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[42]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[43]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[44]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[45]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[46]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[47]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[48]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[49]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[50]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[51]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[52]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[53]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[54]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[55]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[56]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[57]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[58]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[59]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[60]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[61]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[62]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[63]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[64]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[65]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[66]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[67]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[68]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[69]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[70]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[71]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[72]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[73]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[74]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[75]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[76]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[77]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[78]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[79]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[80]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[81]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[82]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[83]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[84]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[85]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[86]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[87]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[88]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[89]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[90]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[91]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[92]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[93]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[94]; sum >= n {
		sum -= n
	}
	a >>= 1
	if sum = sum + (a&0x1)*pow2[95]; sum >= n {
		sum -= n
	}
	return sum
}

// N returns the positive integer that we're reducing by.
func (c *largeN) N() int64 {
	return int64(c.n)
}

// MultiplyUint64 returns (a * b) mod N.
func (c *largeN) MultiplyUint64(a uint64, b uint64) uint64 {
	// Reduce a and b modulo n
	n := c.n
	if a >= n {
		a = a % n
	}
	if b >= n {
		b = b % n
	}
	// Perform the multiplication
	return c.MultiplyReducedUint64(a, b)
}

// MultiplyReducedUint64 returns (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (c *largeN) MultiplyReducedUint64(a uint64, b uint64) uint64 {
	// Make a note of n
	n := c.n
	// Have a go at computing the answer
	k := multiplyEasyReducedUint64(a, b, n)
	if k != n {
		return k
	}
	// No luck -- we need to do this the hard way. Express
	//	a = 2^32 * a1 + a2, 	b = 2^32 * b1 + b2
	// so that
	//	a * b = 2^64 * a1 * b1 + 2^32 * (a1 * b2 + a2 * b1) + a2 * b2
	// where the values a1, a2, b1, b2 are 32-bit integers
	a1, a2 := a>>32, a&((1<<32)-1)
	b1, b2 := b>>32, b&((1<<32)-1)
	a1b1, a1b2, a2b1, a2b2 := a1*b1, a1*b2, a2*b1, a2*b2
	if a1b1 >= n {
		a1b1 = a1b1 % n
	}
	if a1b2 >= n {
		a1b2 = a1b2 % n
	}
	if a2b1 >= n {
		a2b1 = a2b1 % n
	}
	if a2b2 >= n {
		a2b2 = a2b2 % n
	}
	// Compute the sum m := (a1 * b2 + a2 * b1) mod n
	m := a1b2 + a2b1
	if m >= n {
		m -= n
	}
	// Calculate (2^32 * m) mod n
	if mm := multiplyEasyReducedUint64(m, c.pow2[0], n); mm != n {
		m = mm
	} else {
		m = c.multiplyReducedPow32(m)
	}
	// Calculate (2^64 * a1 * b1) mod n
	if k = multiplyEasyReducedUint64(a1b1, c.pow2[32], n); k == n {
		k = c.multiplyReducedPow64(a1b1)
	}
	// Sum it all together
	sum := k + m
	if sum >= n {
		sum -= n
	}
	if sum = sum + a2b2; sum >= n {
		sum -= n
	}
	return sum
}

// MultiplyInt64 returns the product (a * b) mod N.
func (c *largeN) MultiplyInt64(a int64, b int64) int64 {
	// Reduce a and b modulo n
	n := int64(c.n)
	if a < 0 || a >= n {
		a = a % n
	}
	if b < 0 || b >= n {
		b = b % n
	}
	// Perform the multiplication
	return int64(c.MultiplyReducedUint64(uint64(a), uint64(b)))
}

// MultiplyReducedInt64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (c *largeN) MultiplyReducedInt64(a int64, b int64) int64 {
	return int64(c.MultiplyReducedUint64(uint64(a), uint64(b)))
}
