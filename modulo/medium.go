// Medium implements arithmetic modulo n for medium values of n.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package modulo

import (
	"math"
)

// mediumN implements Calculator for medium values of N.
type mediumN int32

/////////////////////////////////////////////////////////////////////////
// mediumN functions
/////////////////////////////////////////////////////////////////////////

// newMediumN returns a Calculator for the given large integer n. Requires that n is positive and fits in an int32, otherwise this will panic.
func newMediumN(n int64) Calculator {
	// Sanity check
	if n <= 0 {
		panic("Argument must be a positive integer")
	} else if n > math.MaxInt32 {
		panic("Argument must fit in an int32")
	}
	// Return the calculator
	return mediumN(n)
}

// N returns the positive integer that we're reducing by.
func (c mediumN) N() int64 {
	return int64(c)
}

// MultiplyUint64 returns (a * b) mod N.
func (c mediumN) MultiplyUint64(a uint64, b uint64) uint64 {
	// Reduce a and b modulo n
	n := uint64(c)
	if a > math.MaxInt32 {
		a = a % n
	}
	if b > math.MaxInt32 {
		b = b % n
	}
	// Perform the multiplication
	return (a * b) % n
}

// MultiplyReducedUint64 returns (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (c mediumN) MultiplyReducedUint64(a uint64, b uint64) uint64 {
	return (a * b) % uint64(c)
}

// MultiplyInt64 returns the product (a * b) mod N.
func (c mediumN) MultiplyInt64(a int64, b int64) int64 {
	// Reduce a and b modulo n
	n := int64(c)
	if a < math.MinInt32 || a > math.MaxInt32 {
		a = a % n
	}
	if b < math.MinInt32 || b > math.MaxInt32 {
		b = b % n
	}
	// Perform the multiplication
	return (a * b) % n
}

// MultiplyReducedInt64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (c mediumN) MultiplyReducedInt64(a int64, b int64) int64 {
	return (a * b) % int64(c)
}
