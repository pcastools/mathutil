// One implements arithmetic modulo 1.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package modulo

// oneN implements a Calculator for N = 1.
type oneN struct{}

/////////////////////////////////////////////////////////////////////////
// oneN functions
/////////////////////////////////////////////////////////////////////////

// newOne returns a Calculator for working modulo 1.
func newOne() Calculator {
	return oneN{}
}

// N returns the positive integer that we're reducing by.
func (_ oneN) N() int64 {
	return 1
}

// MultiplyUint64 returns the product (a * b) mod N.
func (_ oneN) MultiplyUint64(_ uint64, _ uint64) uint64 {
	return 0
}

// MultiplyReducedUint64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (_ oneN) MultiplyReducedUint64(_ uint64, _ uint64) uint64 {
	return 0
}

// MultiplyInt64 returns the product (a * b) mod N.
func (_ oneN) MultiplyInt64(_ int64, _ int64) int64 {
	return 0
}

// MultiplyReducedInt64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
func (_ oneN) MultiplyReducedInt64(_ int64, _ int64) int64 {
	return 0
}
