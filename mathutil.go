// Mathutil does safe arithmetic for the standard integer types.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mathutil

import (
	"math"
)

// MaxInt defines the maximum size of an int.
const MaxInt = int64(int(^uint(0) >> 1))

// MinInt defines the minimum size of an int.
const MinInt = -MaxInt - 1

// MaxUint defines the maximum size of a uint.
const MaxUint = uint64(^uint(0))

/////////////////////////////////////////////////////////////////////////
// Add functions
/////////////////////////////////////////////////////////////////////////

// AddInt8 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddInt8(a int8, b int8) (int8, bool) {
	c := int16(a) + int16(b)
	if c < math.MinInt8 {
		return math.MinInt8, false
	} else if c > math.MaxInt8 {
		return math.MaxInt8, false
	}
	return int8(c), true
}

// AddInt16 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddInt16(a int16, b int16) (int16, bool) {
	c := int32(a) + int32(b)
	if c < math.MinInt16 {
		return math.MinInt16, false
	} else if c > math.MaxInt16 {
		return math.MaxInt16, false
	}
	return int16(c), true
}

// AddInt32 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddInt32(a int32, b int32) (int32, bool) {
	c := int64(a) + int64(b)
	if c < math.MinInt32 {
		return math.MinInt32, false
	} else if c > math.MaxInt32 {
		return math.MaxInt32, false
	}
	return int32(c), true
}

// AddInt64 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddInt64(a int64, b int64) (int64, bool) {
	if a > 0 {
		if b > 0 && math.MaxInt64-a < b {
			return math.MaxInt64, false
		}
	} else if a < 0 && b < 0 && (a == math.MinInt64 || math.MinInt64-a > b) {
		return math.MinInt64, false
	}
	return a + b, true
}

// AddUint8 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddUint8(a uint8, b uint8) (uint8, bool) {
	if math.MaxUint8-a < b {
		return math.MaxUint8, false
	}
	return a + b, true
}

// AddUint16 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddUint16(a uint16, b uint16) (uint16, bool) {
	if math.MaxUint16-a < b {
		return math.MaxUint16, false
	}
	return a + b, true
}

// AddUint32 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddUint32(a uint32, b uint32) (uint32, bool) {
	if math.MaxUint32-a < b {
		return math.MaxUint32, false
	}
	return a + b, true
}

// AddUint64 calculates a+b with accumulation at the maximum and minimum values for their type. That is, it returns a+b,true if the sum lies with the range for the type, and max/min of range,false otherwise.
func AddUint64(a uint64, b uint64) (uint64, bool) {
	if math.MaxUint64-a < b {
		return math.MaxUint64, false
	}
	return a + b, true
}

/////////////////////////////////////////////////////////////////////////
// Subtract functions
/////////////////////////////////////////////////////////////////////////

// SubtractInt8 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractInt8(a int8, b int8) (int8, bool) {
	c := int16(a) - int16(b)
	if c < math.MinInt8 {
		return math.MinInt8, false
	} else if c > math.MaxInt8 {
		return math.MaxInt8, false
	}
	return int8(c), true
}

// SubtractInt16 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractInt16(a int16, b int16) (int16, bool) {
	c := int32(a) - int32(b)
	if c < math.MinInt16 {
		return math.MinInt16, false
	} else if c > math.MaxInt16 {
		return math.MaxInt16, false
	}
	return int16(c), true
}

// SubtractInt32 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractInt32(a int32, b int32) (int32, bool) {
	c := int64(a) - int64(b)
	if c < math.MinInt32 {
		return math.MinInt32, false
	} else if c > math.MaxInt32 {
		return math.MaxInt32, false
	}
	return int32(c), true
}

// SubtractInt64 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractInt64(a int64, b int64) (int64, bool) {
	if a > 0 {
		if b < 0 && (b == math.MinInt64 || a-math.MaxInt64 > b) {
			return math.MaxInt64, false
		}
	} else if a < 0 && b > 0 && (math.MinInt64+b > a) {
		return math.MinInt64, false
	}
	return a - b, true
}

// SubtractUint8 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractUint8(a uint8, b uint8) (uint8, bool) {
	if a < b {
		return 0, false
	}
	return a - b, true
}

// SubtractUint16 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractUint16(a uint16, b uint16) (uint16, bool) {
	if a < b {
		return 0, false
	}
	return a - b, true
}

// SubtractUint32 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractUint32(a uint32, b uint32) (uint32, bool) {
	if a < b {
		return 0, false
	}
	return a - b, true
}

// SubtractUint64 calculates a-b with accumulation at the maximum and minimum values for their type. That is, it returns a-b,true if the difference lies with the range for the type, and max/min of range,false otherwise.
func SubtractUint64(a uint64, b uint64) (uint64, bool) {
	if a < b {
		return 0, false
	}
	return a - b, true
}

/////////////////////////////////////////////////////////////////////////
// Multiply functions
/////////////////////////////////////////////////////////////////////////

// MultiplyInt8 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyInt8(a int8, b int8) (int8, bool) {
	c := int32(a) * int32(b)
	if c < math.MinInt8 {
		return math.MinInt8, false
	} else if c > math.MaxInt8 {
		return math.MaxInt8, false
	}
	return int8(c), true
}

// MultiplyInt16 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyInt16(a int16, b int16) (int16, bool) {
	if a == math.MinInt16 && b == math.MinInt16 {
		return math.MaxInt16, false
	}
	c := int32(a) * int32(b)
	if c < math.MinInt16 {
		return math.MinInt16, false
	} else if c > math.MaxInt16 {
		return math.MaxInt16, false
	}
	return int16(c), true
}

// MultiplyInt32 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyInt32(a int32, b int32) (int32, bool) {
	if a == math.MinInt32 && b == math.MinInt32 {
		return math.MaxInt32, false
	}
	c := int64(a) * int64(b)
	if c < math.MinInt32 {
		return math.MinInt32, false
	} else if c > math.MaxInt32 {
		return math.MaxInt32, false
	}
	return int32(c), true
}

// MultiplyInt64 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyInt64(a int64, b int64) (int64, bool) {
	if a <= math.MinInt32 && b <= math.MinInt32 {
		return math.MaxInt64, false
	} else if (a <= math.MaxInt32 && a >= math.MinInt32 && b <= math.MaxInt32 && b >= math.MinInt32) || a == 0 || a == 1 || b == 0 || b == 1 {
		return a * b, true
	} else if a == -1 {
		if b == math.MinInt64 {
			return math.MaxInt64, false
		}
		return -b, true
	} else if b == -1 {
		if a == math.MinInt64 {
			return math.MaxInt64, false
		}
		return -a, true
	}
	d := a * b
	if d/a != b {
		if (a < 0 && b < 0) || (a > 0 && b > 0) {
			return math.MaxInt64, false
		}
		return math.MinInt64, false
	}
	return d, true
}

// MultiplyUint8 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyUint8(a uint8, b uint8) (uint8, bool) {
	c := uint16(a) * uint16(b)
	if c > math.MaxUint8 {
		return math.MaxUint8, false
	}
	return uint8(c), true
}

// MultiplyUint16 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyUint16(a uint16, b uint16) (uint16, bool) {
	c := uint32(a) * uint32(b)
	if c > math.MaxUint16 {
		return math.MaxUint16, false
	}
	return uint16(c), true
}

// MultiplyUint32 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyUint32(a uint32, b uint32) (uint32, bool) {
	c := uint64(a) * uint64(b)
	if c > math.MaxUint32 {
		return math.MaxUint32, false
	}
	return uint32(c), true
}

// MultiplyUint64 calculates a*b with accumulation at the maximum and minimum values for their type. That is, it returns a*b,true if the product lies with the range for the type, and max/min of range,false otherwise.
func MultiplyUint64(a uint64, b uint64) (uint64, bool) {
	if (a <= math.MaxUint32 && b <= math.MaxUint32) || a == 0 || a == 1 || b == 0 || b == 1 {
		return a * b, true
	}
	d := a * b
	if d/a != b {
		return math.MaxUint64, false
	}
	return d, true
}

// QuotientAndRemainderInt64 returns the quotient q and remainder r such that a = q b+r, where 0 <= r < b if b > 0, and b < r <= 0 if b < 0. This will panic if b is zero.
func QuotientAndRemainderInt64(a int64, b int64) (q int64, r int64) {
	if b == 0 {
		panic("Argument 2 must be non-zero")
	}
	q = a / b
	if ((a < 0 && b > 0) || (a > 0 && b < 0)) && (a%b) != 0 {
		q--
	}
	r = a - q*b
	return
}

// PowerInt64 calculates a^b. It returns a^b,true if the power could be computed, and 0,false otherwise. This will panic if the exponent b is < 0 and a is not a unit. Important: This function guarantees correctness of the result, however a return value of 0,false does NOT necessarily mean that the result doesn't fit in an int64.
func PowerInt64(a int64, b int64) (int64, bool) {
	// Get the easy cases out of the way
	if b < 0 {
		if a == 1 {
			return 1, true
		} else if a == -1 {
			if (b % 2) == 0 {
				return 1, true
			}
			return -1, true
		}
		panic("Illegal negative exponent")
	} else if b == 0 {
		return 1, true
	} else if b == 1 {
		return a, true
	} else if a == 0 {
		return 0, true
	} else if a == 2 {
		// Special case powers of 2
		if b >= 63 {
			return 0, false
		}
		return 1 << uint(b), true
	} else if a == -2 {
		// Spacial case powers of -2
		if b > 63 {
			return 0, false
		} else if b == 63 {
			return math.MinInt64, true
		} else if (b % 2) == 0 {
			return 1 << uint(b), true
		}
		return -(1 << uint(b)), true
	}
	// Do the work
	pow := a
	exp := b
	c := int64(1)
	for exp > 0 {
		var ok bool
		if (exp & 1) != 0 {
			if c, ok = MultiplyInt64(c, pow); !ok {
				return 0, false
			}
		}
		exp >>= 1
		if exp > 0 {
			if pow, ok = MultiplyInt64(pow, pow); !ok {
				return 0, false
			}
		}
	}
	return c, true
}
