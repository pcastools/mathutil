// Modulo implements arithmetic modulo n.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package modulo

import (
	"math"
)

// Calculator is the interface satisfied by an object able to perform arithmetic modulo n.
type Calculator interface {
	N() int64                                        // N returns the positive integer that we're reducing by.
	MultiplyUint64(a uint64, b uint64) uint64        // MultiplyUint64 returns the product (a * b) mod N.
	MultiplyReducedUint64(a uint64, b uint64) uint64 // MultiplyReducedUint64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
	MultiplyInt64(a int64, b int64) int64            // MultiplyInt64 returns the product (a * b) mod N.
	MultiplyReducedInt64(a int64, b int64) int64     // MultiplyReducedInt64 returns the product (a * b) mod N. Assumes that a and b are already reduced mod N (otherwise the result is undefined).
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// multiplyEasyReducedUint64 attempts to compute (a * b) mod n. Returns n if the computation cannot be readily performed. Assumes that a and b are already reduced mod n, and that n fits in an int64.
func multiplyEasyReducedUint64(a uint64, b uint64, n uint64) uint64 {
	// Make sure that a is the smaller of a and b
	if a > b {
		a, b = b, a
	}
	// Do the easy cases
	if b <= math.MaxInt32 {
		return (a * b) % n
	} else if a == 0 {
		return 0
	}
	// Can we use Schrage's method? If not, return n.
	if a > math.MaxInt32 || a*a > n {
		return n
	}
	// Calculate q and r  (r is automatically of the correct sign)
	q := n / a
	r := n - a*q
	// We need [b/q] and b mod q, too. (Again, the sign of b%q is
	// automatically correct.)
	bDivq := b / q
	bModq := b - q*bDivq
	// Do the calculation
	t1, t2 := a*bModq, r*bDivq
	if t1 >= t2 {
		return t1 - t2
	}
	t2 = n - t2
	return t1 + t2
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new calculator for the given value of n. Requires that n is a positive integer, otherwise this will panic.
func New(n int64) Calculator {
	// Sanity check
	if n < 0 {
		panic("Argument must be a positive integer")
	}
	// Create the appropriate calculator based on the size of n
	if n == 1 {
		return newOne()
	} else if n <= math.MaxInt32 {
		return newMediumN(n)
	}
	return newLargeN(n)
}
